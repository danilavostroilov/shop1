package shop.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import shop.model.entities.Role;
import shop.model.entities.User;
import shop.model.repositories.UserRepository;
import java.util.Collections;
import java.util.Map;

@Controller
public class RegistrationController {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    @Autowired
    public RegistrationController(UserRepository userRepository,
                                  PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }
    @GetMapping("/registration")
    public String registration(Model model) {
        User admin = new User();
        model.addAttribute("user", admin);
        return "registration";
    }
    @PostMapping("/registration")
    public String addUser(@ModelAttribute("user") User admin) {
        admin.setActive(true);
        admin.setRoles(Collections.singleton(Role.ADMIN));
        admin.setPassword(passwordEncoder.encode(admin.getPassword()));
        userRepository.save(admin);
        return "redirect:/login";
    }
}
