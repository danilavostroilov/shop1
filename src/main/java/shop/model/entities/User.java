package shop.model.entities;

import lombok.Data;
import javax.persistence.*;
import java.util.Set;
@Entity
@Data
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "active")
    private boolean active;
    @Column(name = "last_name")
    private String last_name;
    @Column(name = "name")
    private String name;
    @Column(name = "second_name")
    private String second_name;
    @Column(name = "appointment")
    private String appointment;

    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Enumerated(EnumType.STRING)
    private Set<Role> roles;
}
